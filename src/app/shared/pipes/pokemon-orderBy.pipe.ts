import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderBy',
    pure: false
})
export class OrderBy implements PipeTransform {
    transform(pokemons: any[], field: string): any {
        if (!pokemons) {
            console.log(pokemons)
            return pokemons;
        }
        return pokemons.sort((a: any, b: any) => a.name.localeCompare(b.name));
    }
}