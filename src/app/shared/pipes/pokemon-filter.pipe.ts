import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pokemonFilter',
    pure: false
})
export class PokemonFilter implements PipeTransform {
    transform(pokemons: any[], filter: Object): any {
        if (!pokemons || !filter) {
            return pokemons;
        }
        return pokemons.filter(item => item.name.indexOf(filter) !== -1);
    }
}