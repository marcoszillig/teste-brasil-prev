import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import { HttpService } from '../http.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public pokemonId;
  pokemon: Object;

  constructor(private route: ActivatedRoute, private router: Router, private _http: HttpService) { }

  ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');

    this.pokemonId = id;

    this._http.getPokemon(this.pokemonId).subscribe(data => {
      this.pokemon = data;
      console.log(this.pokemon);
    });
  }

}
