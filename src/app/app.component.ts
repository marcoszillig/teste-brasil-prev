import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-header></app-header>
    <div class="container">
	    <router-outlet></router-outlet>
    </div>  `,
  styleUrls: []
})

export class AppComponent {
  title = 'brasilprev';
}

