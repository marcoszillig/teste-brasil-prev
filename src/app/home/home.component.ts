import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  pokemons: Object;

  constructor(private _http: HttpService, private router: Router) { }

  ngOnInit() {
    this._http.getPokemons().subscribe(data => {
      this.pokemons = data;
    });
  }

  onSelect(pokemon) {
    this.router.navigate(['/details', pokemon.id]);
  }

}
